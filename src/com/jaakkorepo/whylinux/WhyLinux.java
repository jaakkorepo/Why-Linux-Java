package com.jaakkorepo.whylinux;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextPane;

import org.json.JSONObject;
import java.awt.Font;
import java.awt.Color;

public class WhyLinux {

	private JFrame frmWhyLinux;

	/**
	 * @wbp.nonvisual location=100,391
	 */

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WhyLinux window = new WhyLinux();
					window.frmWhyLinux.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public WhyLinux() {
		initialize();
	}
	
	public static class Chosen {
		public static String id;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmWhyLinux = new JFrame();
		frmWhyLinux.getContentPane().setBackground(Color.DARK_GRAY);
		frmWhyLinux.setBackground(Color.DARK_GRAY);
		frmWhyLinux.setResizable(false);
		frmWhyLinux.setTitle("Why Linux");
		frmWhyLinux.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		frmWhyLinux.setBounds(100, 100, 450, 300);
		frmWhyLinux.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmWhyLinux.getContentPane().setLayout(null);

		JTextPane URLText = new JTextPane();
		URLText.setFont(new Font("Monaco", Font.PLAIN, 16));
		URLText.setForeground(Color.WHITE);
		URLText.setBackground(Color.DARK_GRAY);
		URLText.setEditable(false);
		URLText.setText("Loading...");
		URLText.setBounds(34, 21, 382, 27);
		frmWhyLinux.getContentPane().add(URLText);

		JTextPane DistroDesc = new JTextPane();
		DistroDesc.setFont(new Font("Monaco", Font.PLAIN, 20));
		DistroDesc.setForeground(Color.WHITE);
		DistroDesc.setBackground(Color.DARK_GRAY);
		DistroDesc.setEditable(false);
		DistroDesc.setText("Loading...");
		DistroDesc.setBounds(34, 60, 382, 162);
		frmWhyLinux.getContentPane().add(DistroDesc);
		
				JButton btnMore = new JButton("Loading...");
				btnMore.setBounds(225, 243, 219, 29);
				frmWhyLinux.getContentPane().add(btnMore);
				
				btnMore.addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent e) {
						updateReq(DistroDesc, URLText, btnMore, Chosen.id);
					}
				});
				
				updateReq(DistroDesc, URLText, btnMore, "");
		
				JButton btnRefresh = new JButton("Why Linux");
				btnRefresh.setBounds(6, 243, 219, 29);
				frmWhyLinux.getContentPane().add(btnRefresh);
				btnRefresh.setBackground(Color.DARK_GRAY);
				
						btnRefresh.addMouseListener(new MouseAdapter() {
							@Override
							public void mousePressed(MouseEvent e) {
								updateReq(DistroDesc, URLText, btnMore, "");
							}
						});
	}

	private void updateReq(JTextPane DistroDesc, JTextPane URLText, JButton btnMore, String distro) {
		try {
			JSONObject object = distro.isEmpty() ? HTTP.get("https://linux.jaakkorepo.com/get/random") : HTTP.get("https://linux.jaakkorepo.com/get/" + distro + "/random/random");
			DistroDesc.setText(object.getString("message"));
			URLText.setText("https://linux.jaakkorepo.com" + object.getString("permaLink"));
			btnMore.setText("More " + object.getJSONObject("parts").getJSONObject("distro").getString("string"));
			Chosen.id = Integer.toString(object.getJSONObject("parts").getJSONObject("distro").getInt("id"));
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			DistroDesc.setText("Connection problems...");
			URLText.setText("Connection problems...");
			btnMore.setText("whats happening");
			System.out.println(e1.getMessage());
		}
	}
}
