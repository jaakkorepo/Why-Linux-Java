# LuckyBlocks

Java client for [Why Linux](https://linux.jaakkorepo.com)

Requires Java 1.8.

## Authors

See the list of [contributors](https://github.com/jaqreven/Why-Linux-Java/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
